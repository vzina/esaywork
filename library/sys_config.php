<?php
/**
 * sys_config.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/15
 * Time: 15:48
 */

defined('LIB_PATH') or define('LIB_PATH', __DIR__);
defined('ROOT_PATH') or define('ROOT_PATH', dirname(LIB_PATH));
defined('APP_CONF_PATH') or define('APP_CONF_PATH', ROOT_PATH . '/config/app');
defined('SERVER_CONF_PATH') or define('SERVER_CONF_PATH', ROOT_PATH . '/config/server');
defined('RUNTIME_PATH') or define('RUNTIME_PATH', ROOT_PATH . '/runtime');

date_default_timezone_set('PRC');
require LIB_PATH . '/Swoole/Loader.php';

$loader = new \Swoole\Loader();

$loader->registerNamespace([
    'Swoole' => LIB_PATH . '/Swoole',
    'EasyWork' => LIB_PATH . '/src',
    'App' => ROOT_PATH . '/app'
])->handle();

class EasyWork
{
    public static function createHttpServerHandler(array $config = [])
    {
        \Swoole\SysLog::init($config['log']);
        return new \Swoole\Protocol\Adapter\DefaultAdapter();
    }
}


