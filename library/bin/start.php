#!/usr/bin/env php
<?php
/**
 * start.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/15
 * Time: 15:56
 */
use Swoole\ArgParser;
use Swoole\Exception;
use Swoole\Protocol\ProtocolAbstract;
use Swoole\Server\ServerAbstract;

require dirname(__DIR__) . '/sys_config.php';

$parser = new ArgParser();
$parser->add('cmd', array('help' => 'The cmd of start|stop|restart'));
$parser->add('name', array('help' => 'The name of config filename'));
// Try to parse the arguments
if (!$args = $parser->parse()) {
    print $parser->help();
    exit;
}

//读取配置
$cmd = $args['cmd'];   //cmd name
$name = $args['name'];

//需要cmd 和 name  name 支持 all 和 具体的serverName
if (!$cmd || !$name) {
    echo "please input cmd and server name: start all,start testserv ";
    exit;
}

try {
    //读取配置文件 然后启动对应的server
    $configPath = SERVER_CONF_PATH . '/' . $name . '.php';//获取配置地址
    // $config is file path? 提前读取 只读一次
    if (!file_exists($configPath)) {
        throw new Exception("[error] profiles [$configPath] can not be loaded");
    }
    $_config = Swoole\Loader::import($configPath);
    // Load the configuration file into an array
    //根据config里面的不同内容启动不同的server  定义网络层 UDP、TCP
    if (!($serverType = $_config['server']['type']) && !($serverType instanceof ServerAbstract)) {
        throw new Exception("[error] server type error!");
    }

    /**
     * @var ServerAbstract $sysServer
     */
    $sysServer = new $serverType();
    $sysServer->setConfig($_config)
        ->setCmd($cmd)
        ->setProcessName($name)
        ->setProtocol(EasyWork::createHttpServerHandler($_config))
        ->run();

} catch (\Exception $e) {
    echo str_replace(ROOT_PATH, '', $e->getMessage()) . PHP_EOL;
    exit(1);
}