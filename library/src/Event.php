<?php

namespace EasyWork;



use EasyWork\Event\EventException;
use EasyWork\Event\EventInterface;

/**
 * Creating an Event
 * <?php
 * $event = new EasyWork\Event();
 *
 * Adding Listeners
 * <?php
 * $event->on('user.created', function (User $user) use ($logger) {
 * $logger->log(sprintf("User '%s' was created.", $user->getLogin()));
 * });
 *
 * Emitting Events
 * <?php
 * $event->emit('user.created', array($user));
 *
 * Class Event
 * @package EasyWork
 */
class Event
{
    /**
     * @var EventInterface $_event
     */
    protected $_event;

    public function __construct($options = [])
    {
        $options += [
            'adapter' => 'Local',
        ];
        $eventClass = '\EasyWork\Event\Adapter\\' . ucfirst($options['adapter']);

        $this->_event = new $eventClass();
        if(!($this->_event instanceof EventInterface)){
            throw new EventException('[error] The ' . $eventClass . ' is not instanceof \EasyWork\Event\EventInterface');
        }
    }

    public function __call($method, $args)
    {
        if(!method_exists($this->_event, $method)){
            throw new EventException('[error] The ' . $method . ' is not exist!');
        }
        return call_user_func_array([$this->_event, $method],(array)$args);
    }
}
