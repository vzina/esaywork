<?php
/**
 * Router.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/22
 * Time: 10:18
 */

namespace EasyWork;


class Router
{
    protected static $_namespace = 'App';
    protected static $_module = 'Home';
    protected static $_controller;
    protected static $_controllerSuffix = 'Controller';
    protected static $_action;
    protected static $_actionSuffix = 'Action';

    /**
     * @param callable $routeDefinitionCallback
     * @param array $options
     *
     * @return \EasyWork\Router\Dispatcher
     */
    public static function simpleDispatcher(callable $routeDefinitionCallback, array $options = [])
    {
        $options += [
            'routeParser' => 'EasyWork\\Router\\RouteParser\\Std',
            'dataGenerator' => 'EasyWork\\Router\\DataGenerator\\GroupCountBased',
            'dispatcher' => 'EasyWork\\Router\\Dispatcher\\GroupCountBased',
            'routeCollector' => 'EasyWork\\Router\\RouteCollector',
        ];

        /** @var \EasyWork\Router\RouteCollector $routeCollector */
        $routeCollector = new $options['routeCollector'](
            new $options['routeParser'], new $options['dataGenerator']
        );
        $routeDefinitionCallback($routeCollector);

        return new $options['dispatcher']($routeCollector->getData());
    }

    /**
     * @param callable $routeDefinitionCallback
     * @param array $options
     *
     * @return \EasyWork\Router\Dispatcher
     */
    public static function cachedDispatcher(callable $routeDefinitionCallback, array $options = [])
    {
        $options += [
            'routeParser' => 'EasyWork\\Router\\RouteParser\\Std',
            'dataGenerator' => 'EasyWork\\Router\\DataGenerator\\GroupCountBased',
            'dispatcher' => 'EasyWork\\Router\\Dispatcher\\GroupCountBased',
            'routeCollector' => 'EasyWork\\Router\\RouteCollector',
            'cacheDisabled' => false,
        ];

        if (!isset($options['cacheFile'])) {
            throw new \LogicException('Must specify "cacheFile" option');
        }

        if (!$options['cacheDisabled'] && file_exists($options['cacheFile'])) {
            $dispatchData = require $options['cacheFile'];
            if (!is_array($dispatchData)) {
                throw new \RuntimeException('Invalid cache file "' . $options['cacheFile'] . '"');
            }
            return new $options['dispatcher']($dispatchData);
        }

        $routeCollector = new $options['routeCollector'](
            new $options['routeParser'], new $options['dataGenerator']
        );
        $routeDefinitionCallback($routeCollector);

        /** @var \EasyWork\Router\RouteCollector $routeCollector */
        $dispatchData = $routeCollector->getData();
        file_put_contents(
            $options['cacheFile'],
            '<?php return ' . var_export($dispatchData, true) . ';'
        );

        return new $options['dispatcher']($dispatchData);
    }

    public static function setNamespace($namespace)
    {
        self::$_namespace = $namespace;
    }

    public static function getNamespace()
    {
        return self::$_namespace;
    }

    public static function setModule($module)
    {
        self::$_module = $module;
    }

    public static function getModule()
    {
        return self::$_module;
    }

    public static function setController($controller)
    {
        self::$_controller = $controller;
    }

    public static function getController()
    {
        return self::$_controller;
    }

    public static function setControllerSuffix($controllerSuffix)
    {
        self::$_controllerSuffix = $controllerSuffix;
    }

    public static function getControllerSuffix()
    {
        return self::$_controllerSuffix;
    }

    public static function setAction($action)
    {
        self::$_action = $action;
    }

    public static function getAction()
    {
        return self::$_action;
    }

    public static function setActionSuffix($actionSuffix)
    {
        self::$_actionSuffix = $actionSuffix;
    }

    public static function getActionSuffix()
    {
        return self::$_actionSuffix;
    }
}