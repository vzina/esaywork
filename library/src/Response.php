<?php
/**
 * Response.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/22
 * Time: 11:34
 */

namespace EasyWork;


class Response
{
    /**
     * @var \swoole_http_response
     */
    protected $sw = null;

    /**
     * 结束输出标记
     * @var bool
     */
    private $_endFlag = false;

    /**
     * 分段输出标记
     * @var bool
     */
    private $_writeFlag = false;

    public function setSwResponse($sw = null)
    {
        $this->sw = $sw;
    }

    public function getSwResponse()
    {
        return $this->sw;
    }

    /**
     * 此函数只调用一次
     * @param $data
     * @return $this|bool
     */
    public function end($data)
    {
        if ($this->_endFlag) {
            return false;
        }

        if (is_null($data)) {
            $data = '';
        }

        if ($this->_writeFlag) {
            $this->sw->write($data);
            $data = '';
        }

        $this->sw->end($data);
        $this->_endFlag = true;
        return $this;
    }

    public function write($data)
    {
        $this->sw->write($data);
        $this->_writeFlag = true;
        return $this;
    }

    public function __call($method, $args)
    {
        if (is_callable([$this->sw, $method])) {
            return call_user_func_array([$this->sw, $method], (array)$args);
        }

        throw new EasyWorkException('[error] ' . $method . ' is not callable in \EasyWork\Response!');
    }

    public function clean()
    {
        $this->_endFlag = false;
        $this->_writeFlag = false;
    }
}