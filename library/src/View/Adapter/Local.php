<?php
/**
 * local.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/22
 * Time: 14:37
 */

namespace EasyWork\View\Adapter;


use EasyWork\Loader;
use EasyWork\View\ViewInterface;

class Local implements ViewInterface
{

    public function render($tpl_file, array $var = [])
    {
        extract((array)$var, EXTR_OVERWRITE);
        ob_start();
        ob_implicit_flush(0);
        include $tpl_file;
        return ob_get_clean();
    }

}