<?php
/**
 * ViewException.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/22
 * Time: 14:30
 */

namespace EasyWork\View;


use EasyWork\EasyWorkException;

class ViewException extends EasyWorkException
{

}