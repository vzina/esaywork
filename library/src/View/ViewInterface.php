<?php
/**
 * ViewInterface.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/22
 * Time: 13:56
 */

namespace EasyWork\View;


interface ViewInterface
{
    public function render($tpl_file, array $var = []);
}