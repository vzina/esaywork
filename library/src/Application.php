<?php
/**
 * Application.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/18
 * Time: 16:20
 */

namespace EasyWork;


use App\Controller\IndexController;
use EasyWork\Router\Dispatcher;

final class Application
{
    /**
     * @var Router
     */
    protected $_router;

    /**
     * @var Application
     */
    protected static $_instance;
    /**
     * @var DI
     */
    protected $_di;

    /**
     * @var \EasyWork\Router\Dispatcher
     */
    protected $_dispatcher;

    /**
     * @var \EasyWork\Request
     */
    protected $_request;

    /**
     * @var \EasyWork\Response
     */
    protected $_response;

    /**
     * Constructor
     *
     */
    protected function __construct()
    {

    }

    public function getRequest()
    {
        if (is_null($this->_request)) {
            $this->_request = $this->_di->get('request');
        }
        return $this->_request;
    }

    public function setRequest($request)
    {
        $this->_request = $request;
    }

    public function getResponse()
    {
        if (is_null($this->_response)) {
            $this->_response = $this->_di->get('response');
        }
        return $this->_response;
    }

    public function setResponse($response)
    {
        $this->_response = $response;
    }

    /**
     * Singleton instance
     * @return Application
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function setDispatcher($dispatcher)
    {
        $this->_dispatcher = $dispatcher;
        return $this;
    }

    public function getDispatcher()
    {
        return $this->_dispatcher;
    }


    /**
     * Set Config
     *
     * @param string $name
     * @param mixed $value
     * @param string $delimiter
     * @return \EasyWork\Application
     */
    public static function setConfig($name, $value, $delimiter = '.')
    {
        self::getInstance()->_di->get('config')
            ->set($name, $value, $delimiter);
        return self::$_instance;
    }

    /**
     * Get Config
     *
     * @param $name
     * @param null $default
     * @param string $delimiter
     * @return
     */
    public static function getConfig($name, $default = null, $delimiter = '.')
    {
        return self::getInstance()->_di->get('config')
            ->get($name, $default, $delimiter);
    }

    public function setDI(DI $di)
    {
        $this->_di = $di;
        return $this;
    }

    public function getDI()
    {
        return $this->_di;
    }

    public function handle(DI $di = null)
    {
        if (!is_null($di)) {
            $this->setDI($di);
        }

        if (empty($this->_di)) {
            throw new EasyWorkException('[error] DI is not exist!');
        }

        $request = $this->getRequest();
        $response = $this->getResponse();

        $routeInfo = $request->handle();

        switch ($routeInfo['r_code']) {
            case Dispatcher::NOT_FOUND:
                // ... 404 Not Found
                $response->getSwResponse()->status(404);
                $response->getSwResponse()->end('<h1>404</h1>');
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                // ... 405 Method Not Allowed
                $response->getSwResponse()->status(405);
                $response->getSwResponse()->end('<h1>405</h1>');
                return;
            case Dispatcher::FOUND:
                $response->getSwResponse()->status(200);
                $this->_dispatcher($routeInfo);
                break;
        }
    }

    private function _dispatcher($routeInfo)
    {
        $_handler = ['module' => 'Home', 'controller' => 'Index', 'action' => 'index'];
        $handler = $routeInfo['r_handle'];
        $type = gettype($handler);
        switch ($type) {
            case 'string':
                if (false != strpos($handler, '@')) {
                    list($_handler['module'], $handler) = explode('@', $handler);
                }

                if (false != strpos($handler, '_')) {
                    list($_handler['controller'], $_handler['action']) = explode('_', $handler);
                } elseif (false != strpos($handler, '/')) {
                    list($_handler['controller'], $_handler['action']) = explode('/', $handler);
                }
                break;
            case 'array':
                $_handler = array_merge($_handler, $handler);
                break;
            case 'object':
                if ($handler instanceof \Closure) {
                    $this->_response->end(call_user_func($handler));
                    return;
                }
        }

        empty($_handler['namespace']) || Router::setModule(ucfirst($_handler['namespace']));
        empty($_handler['module']) || Router::setModule(ucfirst($_handler['module']));
        empty($_handler['controller']) || Router::setController(ucfirst($_handler['controller']));
        empty($_handler['action']) || Router::setAction($_handler['action']);

        $controller = Router::getNamespace()
            . '\\' . Router::getModule()
            . '\\' . Router::getControllerSuffix()
            . '\\' . Router::getController()
            . Router::getControllerSuffix();

        $action = Router::getAction() . Router::getActionSuffix();
        /**
         * @var Controller $obj ;
         */
        $obj = new $controller($routeInfo['request'], $this->_response);
        $rel = new \ReflectionClass($obj->setDI($this->_di));

        if ($rel->hasMethod($action)) {
            $method = $rel->getMethod($action);
            if ($method->isPublic() && !$method->isStatic()) {
                if ($scheduler = $this->_di->get('scheduler')) {
                    if ($method->getNumberOfParameters() > 0) {
                        $result = $method->invokeArgs($obj, $routeInfo['r_var']);
                    } else {
                        $result = $method->invoke($obj);
                    }
                    $scheduler->newTask($result)->run();
                    return;
                }
            }
        }
        $this->_response->getSwResponse()->status(404);
        $this->_response->getSwResponse()->end('<h1>404</h1>');
    }

    public function clean()
    {
        $this->_request->clean();
        $this->_response->clean();
    }
}