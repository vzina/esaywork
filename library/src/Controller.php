<?php
/**
 * Controller.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/18
 * Time: 16:28
 */

namespace EasyWork;


use EasyWork\DI\Injection;

abstract class Controller implements Injection
{
    /**
     * @var DI
     */
    protected $_di;

    protected $_autoRender = true;

    protected $_request;

    protected $_response;

    public function __construct($request, $response)
    {
        $this->_request = $request;
        $this->_response = $response;
    }

    public function render($tpl_file, array $var = [])
    {
        $this->setAutoRender(false);
        $var = array_merge($this->getVar(), $var);
        return $this->view->render($tpl_file, $var);
    }

    public function display($tpl_file, array $var = [])
    {
        $this->response->end($this->render($tpl_file, $var));
    }

    public function getVar()
    {
        return $this->view->getVar();
    }

    public function setVar($var, $value)
    {
        $this->view->setVar($var, $value);
        return $this;
    }

    public function setAutoRender($autoRender = true)
    {
        $this->_autoRender = $autoRender;
        return $this;
    }

    public function isAutoRender()
    {
        return $this->_autoRender;
    }

    final public function setDI(DI $di)
    {
        $this->_di = $di;
        return $this;
    }

    final public function getDI()
    {
        return $this->_di;
    }

    public function __get($name)
    {
        if (empty($name)) {
            return null;
        }

        if (!isset($this->$name)) {
            return $this->_di->get($name);
        }

        return $this->$name;
    }
}