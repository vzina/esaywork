<?php
/**
 * Request.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/22
 * Time: 11:34
 */

namespace EasyWork;


class Request
{
    /**
     * @var \swoole_http_request
     */
    protected $_sw = null;

    /**
     * @var \EasyWork\Router\Dispatcher
     */
    protected $_dispatcher = null;

    protected $_params = [];

    public function getParams()
    {
        return $this->_params;
    }

    public function setParams(array $params)
    {
        $this->_params = array_merge($this->_params, $params);
    }

    public function getSwRequest()
    {
        return $this->_sw;
    }

    public function setSwRequest(\swoole_http_request $sw)
    {
        $this->_sw = $sw;
    }

    public function handle()
    {
        if (empty($this->_dispatcher)) {
            throw new EasyWorkException('[error] dispatcher is not exist!');
        }
        $method = $this->_sw->server['request_method'];
        $uri = $this->_sw->server['request_uri'];

        $_routeInfo = $this->_dispatcher->dispatch($method, $uri);
        $routeInfo['r_code'] = $_routeInfo[0];
        $routeInfo['r_handle'] = isset($_routeInfo[1]) ? $_routeInfo[1] : [];
        $routeInfo['r_var'] = isset($_routeInfo[2]) ? $_routeInfo[2] : [];
        $routeInfo['request'] = [
            'uri' => $uri,
            'header' => $this->_sw->header,
            'server' => $this->_sw->server,
            'get' => array_merge(
                isset($this->_sw->get) ? (array)$this->_sw->get : [],
                $routeInfo['r_var']
            ),
            'post' => isset($this->_sw->post) ? $this->_sw->post : [],
            'files' => isset($this->_sw->files) ? $this->_sw->files : [],
            'cookie' => isset($this->_sw->cookie) ? $this->_sw->cookie : [],
            'rawcontent' => $this->_sw->rawContent(),
            'method' => $method,
            'fd' => $this->_sw->fd
        ];
        return $routeInfo;
    }

    public function getDispatcher()
    {
        return $this->_dispatcher;
    }

    public function setDispatcher($dispatcher)
    {
        $this->_dispatcher = $dispatcher;
        return $this;
    }

    public function clean()
    {
        $this->_params = null;
    }
}