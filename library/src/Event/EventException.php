<?php
/**
 * EventException.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/22
 * Time: 11:10
 */

namespace EasyWork\Event;


use EasyWork\EasyWorkException;

class EventException extends EasyWorkException
{

}