<?php
/**
 * View.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/22
 * Time: 13:56
 */

namespace EasyWork;


use EasyWork\DI\Injection;
use EasyWork\View\Adapter\Local;
use EasyWork\View\ViewException;
use EasyWork\View\ViewInterface;

class View
{
    protected $_engineOpt;
    /**
     * @var ViewInterface
     */
    protected $_engine;

    protected $_viewDir;

    protected $_tpl;

    protected $_disable = false;

    protected $_defaultExt = '.phtml';

    protected $_var = [];

    public function setViewDir($viewDir)
    {
        $this->_viewDir = $viewDir;
    }

    public function getViewDir()
    {
        return $this->_viewDir;
    }

    public function registerEngines(array $engineOpt)
    {
        $this->_engineOpt = $engineOpt;
    }

    public function render($tpl_file, array $var = [])
    {
        if($this->_disable){
            return false;
        }

        if(empty($var)){
            $this->_var = $var;
        }

        $this->_tpl = $this->_viewDir . DIRECTORY_SEPARATOR . $tpl_file;

        $ext = pathinfo($this->_tpl, PATHINFO_EXTENSION);

        if(empty($ext)){
            $ext = $this->_defaultExt;
            $this->_tpl .= $this->_defaultExt;
        }

        if(!file_exists($this->_tpl)){
            throw new ViewException('[error] '.$this->_tpl.' is not exist!');
        }

        if (!empty($this->_engineOpt[$ext])) {
            if (is_string($this->_engineOpt[$ext])) {
                $this->_engine = new $this->_engineOpt[$ext];
                $this->_defaultExt = '.' . $ext;
            } elseif ($this->_engineOpt[$ext] instanceof \Closure) {
                $this->_engine = call_user_func($this->_engineOpt[$ext]);
                $this->_defaultExt = '.' . $ext;
            }

            if (!$this->_engine && !($this->_engine instanceof ViewInterface)) {
                throw new ViewException('[error] Engine is not instanceof EasyWork\View\ViewInterface');
            }
        } else {
            $this->_engine = new Local();
        }

        return $this->_engine->render($this->_tpl, $this->_var);
    }

    public function isDisable()
    {
        $this->_disable = true;
    }

    public function getDefaultExt()
    {
        return $this->_defaultExt;
    }

    public function setDefaultExt($defaultExt)
    {
        $this->_defaultExt = $defaultExt;
    }

    public function getVar()
    {
        return $this->_var;
    }

    public function setVar($var, $value)
    {
        $this->_var[$var] = $value;
    }
}