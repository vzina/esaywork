<?php
/**
 * BaseAdapter.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/15
 * Time: 12:09
 */

namespace Swoole\Protocol\Adapter;


use Swoole\Protocol\ProtocolAbstract;

class BaseAdapter extends ProtocolAbstract
{
    function onStart(\swoole_server $server, $workerId)
    {
        // TODO: Implement onStart() method.
    }

    function onConnect(\swoole_server $server, $client_id, $from_id)
    {
        // TODO: Implement onConnect() method.
    }

    function onReceive(\swoole_server $server, $client_id, $from_id, $data)
    {
        // TODO: Implement onReceive() method.
    }

    function onClose(\swoole_server $server, $client_id, $from_id)
    {
        // TODO: Implement onClose() method.
    }

    function onShutdown(\swoole_server $server, $worker_id)
    {
        // TODO: Implement onShutdown() method.
    }

    function onTask(\swoole_server $server, $task_id, $from_id, $data)
    {
        // TODO: Implement onTask() method.
    }

    function onFinish(\swoole_server $server, $task_id, $data)
    {
        // TODO: Implement onFinish() method.
    }

    function onTimer(\swoole_server $server, $interval)
    {
        // TODO: Implement onTimer() method.
    }
}