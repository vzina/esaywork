<?php
/**
 * DefaultAdapter.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/15
 * Time: 15:53
 */

namespace Swoole\Protocol\Adapter;


use EasyWork\Application;
use Swoole\Config;
use EasyWork\DI;
use Swoole\Client\Adapter\Mysql;
use Swoole\Coroutine\Scheduler;
use Swoole\Loader;

class DefaultAdapter extends BaseAdapter
{
    /**
     * @var DI
     */
    protected $di;

    /**
     * @var Application
     */
    protected $app;

    private $defaultFiles = array('index.html', 'main.html', 'default.html');
    private $mimes = array(
        'jpg' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'bmp' => 'image/bmp',
        'ico' => 'image/x-icon',
        'gif' => 'image/gif',
        'png' => 'image/png',
        'bin' => 'application/octet-stream',
        'js' => 'application/javascript',
        'css' => 'text/css',
        'html' => 'text/html',
        'xml' => 'text/xml',
        'tar' => 'application/x-tar',
        'ppt' => 'application/vnd.ms-powerpoint',
        'pdf' => 'application/pdf',
        'swf' => 'application/x-shockwave-flash',
        'zip' => 'application/x-zip-compressed',
    );

    /**
     * @param \swoole_http_request $request
     * @param \swoole_http_response $response
     */
    public function onRequest($request, $response)
    {
        if ($this->doStaticFile($request, $response)) {
            return;
        }

        $this->setGlobal($request);
        try {
            $this->di->get('request')->setSwRequest($request);
            $this->di->get('response')->setSwResponse($response);
            $this->app->handle($this->di);
            //清除请求信息
            $this->app->clean();
        } catch (\Exception $e) {
            $response->end("<pre>#Msg " . $e->getMessage() . "</pre>");
        }
        $this->unsetGlobal();
    }

    /**
     * [onStart 协程调度器单例模式]
     * @param \swoole_server $server
     * @param $workerId
     */
    public function onStart(\swoole_server $server, $workerId)
    {
        /* 设置只读模式的配置对象 */
        $this->di = Loader::import(APP_CONF_PATH . '/service.php');
        $this->app = Application::getInstance();
    }

    public function onClose(\swoole_server $server, $client_id, $from_id)
    {

    }

    /**
     * 兼容全局变量（$_GET/$_POST/$_REQUEST）
     * @param \swoole_http_request $request
     */
    protected function setGlobal(\swoole_http_request $request)
    {
        if (!empty($request->get)) $_GET = $request->get;
        if (!empty($request->post)) $_POST = $request->post;
        if (!empty($request->files)) $_FILES = $request->files;
        if (!empty($request->cookie)) $_COOKIE = $request->cookie;
        if (!empty($request->server)) $_SERVER = array_change_key_case($request->server, CASE_UPPER);

        $_REQUEST = array_merge($_GET, $_POST, $_COOKIE);
        /**
         * 将HTTP头信息赋值给$_SERVER超全局变量
         */
        foreach ($request->header as $key => $value) {
            $_key = 'HTTP_' . strtoupper(str_replace('-', '_', $key));
            $_SERVER[$_key] = $value;
        }
    }

    /**
     * 清除全局变量数据
     */
    protected function unsetGlobal()
    {
        $_REQUEST = $_COOKIE = $_FILES = $_POST = $_SERVER = $_GET = [];
    }

    /**
     * 静态文件处理;
     * @param \swoole_http_request $request
     * @param \swoole_http_response $response
     * @return bool
     */
    protected function doStaticFile($request, $response)
    {
        $pathInfo = $request->server['path_info'];

        if ($pathInfo == '/') {
            if (!empty($this->defaultFiles)) {
                foreach ($this->defaultFiles as $file) {
                    $staticFile = $this->getStaticFile(DIRECTORY_SEPARATOR . $file);
                    if (is_file($staticFile)) {
                        $response->end(file_get_contents($staticFile));
                        return true;
                    }
                }
            }
        }

        $staticFile = $this->getStaticFile($pathInfo);
        if (is_dir($staticFile)) { //是目录
            foreach ($this->defaultFiles as $file) {
                if (is_file($staticFile . $file)) {
                    $response->header('Content-Type', 'text/html');
                    $response->end(file_get_contents($staticFile . $file));
                    return true;
                }
            }
        }

        $ext = pathinfo($pathInfo, PATHINFO_EXTENSION);
        if (isset($this->mimes[$ext])) {  //非法的扩展名
            if (is_file($staticFile)) { //读取静态文件
                $response->header('Content-Type', $this->mimes[$ext]);
                $response->end(file_get_contents($staticFile));
                return true;
            } else {
                $response->status(404);
                $response->end('');
                return true;
            }
        }
        return false;
    }

    private function getStaticFile($file, $path = 'public')
    {
        return ROOT_PATH . DIRECTORY_SEPARATOR . $path . $file;
    }
}