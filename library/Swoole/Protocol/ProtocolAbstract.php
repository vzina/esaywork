<?php
/**
 * ProtocolAbstract.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/15
 * Time: 12:04
 */

namespace Swoole\Protocol;


abstract class ProtocolAbstract
{

    /**
     * @var \swoole_server $server
     */
    protected $server;
    /**
     * @var \Hoolai\Config $config
     */
    protected $config;

    public function init(){}

    public function setSw($server)
    {
        $this->server = $server;
        return $this;
    }

    public function setSysConfig($config = [])
    {
        $this->config = $config;
        $this->init();
        return $this;
    }
    /**
     * 打印Log信息
     * @param $msg
     * @param string $type
     */
    public function log($msg)
    {
        $log = "[" . date("Y-m-d G:i:s") . " " . floor(microtime() * 1000) . "]" . $msg;
        echo $log, PHP_EOL;
    }

    abstract function onStart(\swoole_server $server, $workerId);

    abstract function onConnect(\swoole_server $server, $client_id, $from_id);

    abstract function onReceive(\swoole_server $server, $client_id, $from_id, $data);

    abstract function onClose(\swoole_server $server, $client_id, $from_id);

    abstract function onShutdown(\swoole_server $server, $worker_id);

    abstract function onTask(\swoole_server $server, $task_id, $from_id, $data);

    abstract function onFinish(\swoole_server $server, $task_id, $data);

    abstract function onTimer(\swoole_server $server, $interval);
}