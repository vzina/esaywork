<?php
/**
 * TCPAdapter.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/15
 * Time: 12:01
 */

namespace Swoole\Server\Adapter;


use Swoole\Server\ServerAbstract;

class TCPAdapter extends ServerAbstract
{
    protected $sockType = SWOOLE_SOCK_TCP;

    public $setting = [
        //      'open_cpu_affinity' => 1,
        'open_tcp_nodelay' => 1,
    ];
}