<?php
/**
 * UDPAdapter.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/18
 * Time: 16:05
 */

namespace Swoole\Server\Adapter;


use Swoole\Server\ServerAbstract;

class UDPAdapter extends ServerAbstract
{
    protected $sockType = SWOOLE_SOCK_UDP;

    public $setting = [];// udp server 默认配置
}