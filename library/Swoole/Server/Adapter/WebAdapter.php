<?php
/**
 * WebAdapter.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/21
 * Time: 11:22
 */

namespace Swoole\Server\Adapter;


use Swoole\Http\Parser;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\Server\ServerAbstract;

class WebAdapter extends ServerAbstract
{
    const SOFTWARE = "SwooleFramework";
    const POST_MAXSIZE = 2000000; //POST最大2M
    const DEFAULT_PORT = 8888;
    const DEFAULT_HOST = '0.0.0.0';

    protected $sockType = SWOOLE_SOCK_TCP;

    public $setting = [
        //      'open_cpu_affinity' => 1,
        'open_tcp_nodelay' => 1,
    ];
    /**
     * @var \Swoole\Http\Parser
     */
    protected $parser;

    protected $keepalive = false;
    protected $gzip = false;
    protected $expire = false;

    protected $requests = array(); //保存请求信息,里面全部是Request对象

    protected $buffer_header = array();
    protected $buffer_maxlen = 65535; //最大POST尺寸，超过将写文件

    const DATE_FORMAT_HTTP = 'D, d-M-Y H:i:s T';

    const HTTP_EOF = "\r\n\r\n";
    const HTTP_HEAD_MAXLEN = 8192; //http头最大长度不得超过2k

    const ST_FINISH = 1; //完成，进入处理流程
    const ST_WAIT = 2; //等待数据
    const ST_ERROR = 3; //错误，丢弃此包

    protected function _initRunTime()
    {
        parent::_initRunTime();
        if (!$this->enableHttp) {
            //开启http keepalive
            if ($this->config->get('server.keepalive')) {
                $this->keepalive = true;
            }
            //是否压缩
            if ($this->config->get('server.gzip_open') and function_exists('gzdeflate')) {
                $this->gzip = true;
            }
            //过期控制
            if ($this->config->get('server.expire_open')) {
                $this->expire = true;
            }
            $this->parser = new Parser;
        }
    }


    public function onShutdown(\swoole_server $server, $worker_id)
    {
        $this->log(self::SOFTWARE . " shutdown");
    }

    public function onConnect(\swoole_server $server, $client_id, $from_id)
    {
        $this->log("Event: client[#$client_id@$from_id] connect");
        $this->protocol->onConnect($server, $client_id, $from_id);
    }

    public function onClose(\swoole_server $server, $client_id, $from_id)
    {
        $this->log("Event: client[#$client_id@$from_id] close");
        $this->enableHttp ?: $this->cleanBuffer($client_id);
        $this->protocol->onClose($server, $client_id, $from_id);
    }

    public function cleanBuffer($fd)
    {
        unset($this->requests[$fd], $this->buffer_header[$fd]);
    }

    protected function checkHeader($client_id, $http_data)
    {
        //新的连接
        if (!isset($this->requests[$client_id])) {
            if (!empty($this->buffer_header[$client_id])) {
                $http_data = $this->buffer_header[$client_id] . $http_data;
            }
            //HTTP结束符
            $ret = strpos($http_data, self::HTTP_EOF);
            //没有找到EOF，继续等待数据
            if ($ret === false) {
                return false;
            } else {
                $this->buffer_header[$client_id] = '';
                $request = new Request;
                //GET没有body
                list($header, $request->body) = explode(self::HTTP_EOF, $http_data, 2);
                $request->head = $this->parser->parseHeader($header);
                //使用head[0]保存额外的信息
                $request->meta = $request->head[0];
                unset($request->head[0]);
                //保存请求
                $this->requests[$client_id] = $request;
                //解析失败
                if ($request->head == false) {
                    $this->log("parseHeader failed. header=" . $header);
                    return false;
                }
            }
        } //POST请求需要合并数据
        else {
            $request = $this->requests[$client_id];
            $request->body .= $http_data;
        }
        return $request;
    }

    protected function checkPost($request)
    {
        if (isset($request->head['Content-Length'])) {
            //超过最大尺寸
            if (intval($request->head['Content-Length']) > $this->config->get('server.post_maxsize', self::POST_MAXSIZE)) {
                $this->log("checkPost failed. post_data is too long.");
                return self::ST_ERROR;
            }
            //不完整，继续等待数据
            if (intval($request->head['Content-Length']) > strlen($request->body)) {
                return self::ST_WAIT;
            } //长度正确
            else {
                return self::ST_FINISH;
            }
        }
        $this->log("checkPost fail. Not have Content-Length.");
        //POST请求没有Content-Length，丢弃此请求
        return self::ST_ERROR;
    }

    protected function checkData($client_id, $http_data)
    {
        if (isset($this->buffer_header[$client_id])) {
            $http_data = $this->buffer_header[$client_id] . $http_data;
        }
        //检测头
        $request = $this->checkHeader($client_id, $http_data);
        //错误的http头
        if ($request === false) {
            $this->buffer_header[$client_id] = $http_data;
            //超过最大HTTP头限制了
            if (strlen($http_data) > self::HTTP_HEAD_MAXLEN) {
                $this->log("http header is too long.");
                return self::ST_ERROR;
            } else {
                $this->log("wait request data. fd={$client_id}");
                return self::ST_WAIT;
            }
        }
        //POST请求需要检测body是否完整
        if ($request->meta['method'] == 'POST') {
            return $this->checkPost($request);
        } //GET请求直接进入处理流程
        else {
            return self::ST_FINISH;
        }
    }

    /**
     * 接收到数据
     * @param \swoole_server $server
     * @param $client_id
     * @param $from_id
     * @param $data
     * @return null
     */
    public function onReceive(\swoole_server $server, $client_id, $from_id, $data)
    {
        if($this->enableHttp){
            $this->protocol->onReceive($server, $client_id, $from_id, $data);
            return;
        }
        //检测request data完整性
        $ret = $this->checkData($client_id, $data);
        switch ($ret) {
            //错误的请求
            case self::ST_ERROR;
                $server->close($client_id);
                return;
            //请求不完整，继续等待
            case self::ST_WAIT:
                return;
            default:
                break;
        }
        /**
         * 完整的请求,开始处理
         * @var $request Request
         */
        $request = $this->requests[$client_id];
        $info = $this->sw->connection_info($client_id);
        $request->remote_ip = $info['remote_ip'];
        $request->remote_port = $info['remote_port'];
        $_SERVER['SWOOLE_CONNECTION_INFO'] = $info;
        $this->parseRequest($request);
        $request->fd = $client_id;

        //处理请求，产生response对象
        $response = new Response;

        $this->onRequest($request, $response);

        if ($response instanceof Response) {
            //发送response
            $this->response($request, $response);
        }
    }

    protected function afterResponse(Request $request, Response $response)
    {
        if (!$this->keepalive or $response->head['Connection'] == 'close') {
            $this->sw->close($request->fd);
        }
        $request->unsetGlobal();
        //清空request缓存区
        unset($this->requests[$request->fd]);
        unset($request);
        unset($response);
    }

    /**
     * 解析请求
     * @param  Request $request
     * @return null
     */
    protected function parseRequest($request)
    {
        $url_info = parse_url($request->meta['request_uri']);
        $request->time = time();
        $request->meta['path_info'] = $url_info['path'];
        if (isset($url_info['fragment'])) $request->meta['fragment'] = $url_info['fragment'];
        if (isset($url_info['query'])) {
            $request->meta['query_string'] = $url_info['query'];
            parse_str($url_info['query'], $request->get);
        }
        //POST请求,有http body
        if ($request->meta['method'] === 'POST') {
            $this->parser->parseBody($request);
        }
        //解析Cookies
        if (!empty($request->head['Cookie'])) {
            $this->parser->parseCookie($request);
        }
    }

    /**
     * 发送响应
     * @param $request Request
     * @param $response Response
     * @return bool
     */
    protected function response(Request $request, Response $response)
    {
        if (!isset($response->head['Date'])) {
            $response->head['Date'] = gmdate("D, d M Y H:i:s T");
        }
        if (!isset($response->head['Connection'])) {
            //keepalive
            if ($this->keepalive
                && (isset($request->head['Connection'])
                    && strtolower($request->head['Connection']) == 'keep-alive')
            ) {
                $response->head['KeepAlive'] = 'on';
                $response->head['Connection'] = 'keep-alive';
            } else {
                $response->head['KeepAlive'] = 'off';
                $response->head['Connection'] = 'close';
            }
        }
        //过期命中
        if ($this->expire and $response->http_status == 304) {
            $out = $response->getHeader();
            return $this->sw->send($request->fd, $out);
        }
        //压缩
        if ($this->gzip) {
            $response->head['Content-Encoding'] = 'deflate';
            $response->body = gzdeflate($response->body, (int)$this->config->get('server.gzip_level', 1));
        }
        $out = $response->getHeader() . $response->body;
        $ret = $this->sw->send($request->fd, $out);
        $this->afterResponse($request, $response);
        return $ret;
    }
}