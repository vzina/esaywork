<?php
/**
 * HTTP.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/15
 * Time: 12:02
 */

namespace Swoole\Server\Adapter;




class HTTPAdapter extends TCPAdapter
{
    public function init()
    {
        $this->enableHttp = true;
    }
}