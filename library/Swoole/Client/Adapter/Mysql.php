<?php
/**
 * Mysql.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/15
 * Time: 17:10
 */

namespace Swoole\Client\Adapter;


use Swoole\Client\IBase;

class Mysql implements IBase
{
    protected $db;
    protected $sql;
    protected $key;
    protected $conf;
    protected $callback;
    protected $calltime;

    /**
     * sqlConf = array(
     * 'host' => ,
     * 'port' => ,
     * 'user' => ,
     * 'psw' => ,
     * 'database' => ,
     * 'charset' => ,
     * );
     * [__construct 构造函数，初始化mysqli]
     * @param [type] $sqlConf [description]
     */
    public function __construct($sqlConf)
    {
        $this->db = new \mysqli();
        $this->conf = $sqlConf;
    }


    /**
     * [send 兼容Base类封装的send方法，调度器可以不感知client类型]
     * @param callable $callback
     */
    public function send(callable $callback)
    {
        if (!isset($this->db)) {
            echo " db not init \n";
            return;
        }

        $config = $this->conf;
        $this->callback = $callback;
        $this->calltime = microtime(true);
        $this->key = md5($this->calltime . $config['host'] . $config['port'] . rand(0, 10000));

        $this->db->connect($config['host'], $config['user'], $config['password'], $config['database'], $config['port']);

        if (!empty($config['charset'])) {
            $this->db->set_charset($config['charset']);
        }

        \swoole_mysql_query($this->db, $this->sql, [$this, 'onSqlReady']);
    }

    /**
     * [query 使用者调用该接口，返回当前mysql实例]
     * @param string $sql
     * @return \Generator
     */
    public function query($sql)
    {
        $this->sql = $sql;
        return $this;
    }

    /**
     * [onSqlReady eventloog异步回调函数]
     * @param \mysqli $db
     * @param mixed $r
     */
    public function onSqlReady(\mysqli $db, $r)
    {
        $res = $r;//执行成功，$r是结果集数组
        if ($r === false) {
            $res = ['msg' => $db->_error, 'code' => $db->_errno];
        } //执行成功，update/delete/insert语句，没有结果集
        elseif ($r === true) {
            $res = ['affected_rows' => $db->_affected_rows, 'insert_id' => $db->_insert_id];
        }

        call_user_func_array($this->callback, [
            'r' => 0,
            'key' => $this->key,
            'calltime' => $this->calltime,
            'data' => $res
        ]);
    }
}