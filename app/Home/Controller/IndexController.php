<?php
namespace App\Home\Controller;

use Swoole\Client\Adapter\Http;
use Swoole\Client\Adapter\Mysql;
use Swoole\Client\Adapter\Redis;

/**
 * IndexController.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/22
 * Time: 15:24
 */
class IndexController extends \EasyWork\Controller
{

    public function indexAction()
    {
        $data1 = (yield $this->mysql());
        $data = (yield $this->redis());
        $data2 = (yield $this->httpTest());
        $this->setVar('aaa', var_export($data, true).var_export($data1, true).var_export($data2['data']['body'], true));
        $this->display('Index/index');
        yield;
    }

    public function redis()
    {
        /** @var \Redis $redis */
        $redis = new Redis();

        yield $redis->set('test', '123456789adfdfdfa');
        yield $redis->get('test');
    }

    public function mysql()
    {
        $mysql = new Mysql([
            'host' => '127.0.0.1',
            'port' => '3306',
            'user' => 'root',
            'password' => 'root',
            'database' => 'test',
            'charset' => 'utf8',
        ]);
        yield $mysql->query('select count(1) as cnt from documents;');
    }

    public function httpTest(){
        $url='http://120.24.48.77/';
        $httpRequest= new Http($url);
        $data='testdata';
        $header = array(
            'Content-Length' => 12345,
        );
        yield $httpRequest->get($url); //yield $httpRequest->post($path, $data, $header);
    }
}