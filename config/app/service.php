<?php
/**
 * service.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/22
 * Time: 12:16
 */

$di = EasyWork\DI::factory();

$di->setShare('config', function () {
    return EasyWork\Loader::import(APP_CONF_PATH . '/config.php');
});

$di->setShare('view', function () {
    $view = new \EasyWork\View();
    $module = \EasyWork\Router::getModule();
    $view->setViewDir(ROOT_PATH . '/app/' . $module . '/View');
    return $view;
});

$di->setShare('response', function () {
    return new \EasyWork\Response();
});

$di->setShare('request', function () {
    $dispatcher = EasyWork\Router::simpleDispatcher(function (EasyWork\Router\RouteCollector $r) {
        $routes = EasyWork\Loader::import(APP_CONF_PATH . '/router.php');
        foreach ($routes as $route) {
            if (empty($route['method'])
                || empty($route['uri'])
                || empty($route['handle'])
            ) {
                continue;
            }
            $r->addRoute($route['method'], $route['uri'], $route['handle']);
        }
    });
    $request = new \EasyWork\Request();
    $request->setDispatcher($dispatcher);
    return $request;
});

$di->setShare('scheduler', function () {
    return new \EasyWork\Coroutine\Scheduler();
});

return $di;