<?php
/**
 * router.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/22
 * Time: 11:43
 */
return [
    [
        'method' => ['GET', 'POST'],
        'uri' => '/users',
        'handle' => function () {
            return 111;
        }
    ],
    [
        'method' => ['GET'],
        'uri' => '/[{a}]',
        'handle' => 'index_index'
    ],
    [
        'method' => ['GET'],
        'uri' => '/articles/{id:\d+}[/{title}]',
        'handle' => ['controller' => 'Index', 'action' => 'index']
    ],
];