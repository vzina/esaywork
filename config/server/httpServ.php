<?php
/**
 * httpServ.php.
 * Author: yeweijian
 * E-mail: yeweijian@hoolai.com
 * Date: 2016/1/18
 * Time: 11:36
 */
use Swoole\Protocol\Adapter\DefaultAdapter;
use Swoole\Server\Adapter\HTTPAdapter;

return [
    'server' => [
        /* php start.php */
        'type' => HTTPAdapter::class,
        'listen' => [
            '192.168.15.234:9501'
        ],
        'protocol' => DefaultAdapter::class,
        'auto_load' => 1,

        /* php swoole.php */
        'php' => '/usr/local/php/bin/php',
    ],
    /* swoole setting */
    'setting' => [
        'worker_num' => 4,
        'task_worker_num' => 0,
        'dispatch_mode' => 2,
        'daemonize' => 0,
        //post max length
        'package_max_length' => 12582912,
        'buffer_output_size' => 12582912,
        // log path
        'log_file' => RUNTIME_PATH . '/logs/swoole' . date('Y-m-d') . '.log',

        'heartbeat_check_interval' => 60,
        'heartbeat_idle_time' => 600,

        //close eof check
        'open_eof_check' => false,
        'open_eof_split' => false,
    ],
    'log' => [
        'log_path' => '',
        'log_level' => []
    ]
];